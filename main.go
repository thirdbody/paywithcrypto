package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/lib"
	"gitlab.com/myl0g/paywithcrypto/wallet"
)

func main() {
	args := os.Args[1:]

	if len(args) <= 0 {
		fmt.Println("No command-line arguments given.")
		os.Exit(1)
	}

	changed, err := config.Setup()
	if err != nil {
		fmt.Println(err)
		os.Exit(3)
	}
	if changed {
		fmt.Println("Created sample configuration files under " + config.GetConfigDirectory())
	}

	switch args[0] {
	case "generate":
		if len(args) < 3 {
			fmt.Println("Not enough arguments given to this command.")
			fmt.Println("Usage: generate [currency] [requested amount]")
			os.Exit(1)
		}

		f, err := strconv.ParseFloat(args[2], 64)
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}

		address, err := wallet.Generate(args[1], f)
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}

		fmt.Println("Generated " + args[1] + " address of " + address.Address)
	case "refresh":
		if err := lib.Refresh(); err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
	case "cashout":
		cashedAddresses, err := lib.Cashout()
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}

		totalAmounts := make(map[string]float64)
		for _, address := range cashedAddresses {
			totalAmounts[address.CurrencyName] += address.PaidAmount
		}

		for currency, totalAmount := range totalAmounts {
			fmt.Printf("Redeemed %f of %s\n", totalAmount, currency)
		}
	case "version":
		fallthrough
	case "-v":
		fmt.Println("0.0.1")
	}
}
