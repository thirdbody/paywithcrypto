package wallet

import (
	"os/exec"
	"strings"
	"time"

	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/definitions"
)

// Generate produces a newly-minted CryptoAddress of the requested currency. It is returned as well as stored in addresses.json.
func Generate(currency string, requestedAmount float64) (definitions.CryptoAddress, error) {
	conf, err := config.GetCurrencyConfig(currency)
	if err != nil {
		return definitions.CryptoAddress{}, err
	}

	command := strings.Split(conf.GenerateAddressCmd, " ")
	address, err := exec.Command(command[0], command[1:]...).Output()
	if err != nil {
		return definitions.CryptoAddress{}, err
	}

	encapsulated := definitions.CryptoAddress{
		CurrencyName:    currency,
		Address:         strings.TrimSuffix(string(address[:]), "\n"),
		CreationDate:    time.Now(),
		RequestedAmount: requestedAmount,
	}

	allAddresses, err := config.GetCryptoAddresses()
	if err != nil {
		return definitions.CryptoAddress{}, err
	}

	return encapsulated, config.SaveJSON(append(allAddresses, encapsulated), config.GetConfigDirectory()+"addresses.json")
}
