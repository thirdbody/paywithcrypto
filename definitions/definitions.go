package definitions

import "time"

// CryptoAddress encapsulates a cryptocurrency address, providing the currency's name in addition to an address associated with it.
type CryptoAddress struct {
	CurrencyName    string    `json:"currencyName"`
	Address         string    `json:"address"`
	CreationDate    time.Time `json:"creationDate"`
	RequestedAmount float64   `json:"requestedAmount"`
	PaidAmount      float64   `json:"paidAmount"`
	InvoiceNumber   uint64    `json:"invoiceNumber"`
}

// CryptocurrencyConfig stores the data needed for paywithcrypto to work with a given cryptocurrency.
type CryptocurrencyConfig struct {
	Name               string `json:"name"`
	GenerateAddressCmd string `json:"generateAddressCmd"`
	SendAllCmd         string `json:"sendAllCmd"`
	GetBalanceCmd      string `json:"getBalanceCmd"`
}
